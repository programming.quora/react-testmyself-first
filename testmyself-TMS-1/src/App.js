import './App.css';
import QuizComponent from './containers/QuizComponent';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

function App() {
  return (
    <div className="App">
      <Router>
        <h1>Test MySelf</h1>
        <Routes>
          <Route path="/quiz/:quizId" exact element= { <QuizComponent/> }/>
          <Route>404 Not found!</Route>
        </Routes>
      </Router>
    </div>
  );
}

export default App;
