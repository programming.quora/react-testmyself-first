import React from 'react';
import QuestionsList from './QuestionsList';

const QuizComponent = () => {
    return (
        <div>
            <QuestionsList/>
        </div>
    );
};

export default QuizComponent;