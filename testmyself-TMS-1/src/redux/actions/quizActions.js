import { ActionTypes } from "../constants/action-types";

export const setQuiz = (questions) => {
    return {
        type: ActionTypes.SET_QUIZ,
        payload: questions,
    };
};

export const setCurrentQuestion = (question) => {
    return {
        type: ActionTypes.SET_CURRENT_QUESTION,
        payload: question,
    };
};

export const setQuizScore = (score) => {
    return {
        type: ActionTypes.SET_QUIZ_SCORE,
        payload: score,
    };
};

export const setSelectedOption = (option) => {
    return {
        type: ActionTypes.SET_SELECTED_OPTION,
        payload: option,
    }
}

export const setCurrentIndex = (index) => {
    return {
        type: ActionTypes.SET_CURRENT_INDEX,
        payload: index,
    }
}

export const toggleSubmit = (value) => {
    return {
        type: ActionTypes.TOGGLE_SUBMIT,
        payload: value,
    }
}

export const setOptionStatus = (status) => {
    return {
        type: ActionTypes.SET_OPTION_STATUS,
        payload: status,
    }
}