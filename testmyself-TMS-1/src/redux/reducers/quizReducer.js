import { ActionTypes } from '../constants/action-types';

const quizInitialState = {
    questionsList:{},
    questionsLength:0,
    quizScores:[],
}
const questionInitialState = {
    currentQuestion:{},
    currentIndex:0,
    checkedOptionId:0,
    submitDisabled:true,
    questionStatus:'unset',
}
export const quizReducer = (state = quizInitialState, {type, payload}) => {
    switch (type) {
        case ActionTypes.SET_QUIZ:
            return {...state, questionsList: payload, questionsLength: payload.questions.length};
        default:
            return state;
    }
};

export const questionReducer = (state = questionInitialState, {type, payload}) => {
    switch (type) {
        case ActionTypes.SET_CURRENT_QUESTION:
            return {...state, currentQuestion: payload};
        case ActionTypes.SET_CURRENT_INDEX:
            return {...state, currentIndex: payload}
        case ActionTypes.SET_SELECTED_OPTION:
            return {...state, checkedOptionId: payload};
        case ActionTypes.TOGGLE_SUBMIT:
            return {...state, submitDisabled: payload};
        case ActionTypes.SET_OPTION_STATUS:
            let {currentQuestion} = state;
            let setStatus = 'unset';
            if(payload.status == 'correct'){
                setStatus = 'disable';
            }
            const newOptions = currentQuestion.options.map(option => {
                if(option.id == payload.id){
                    return {...option, status: payload.status}
                }
                else{
                    if(option.status == 'unset')
                        return {...option, status: setStatus}
                    else
                        return option;
                    // return {...option, status: 'disable'}
                }
            });
            let newQuestion = {...currentQuestion, options:newOptions}
            return {...state, currentQuestion: newQuestion, questionStatus:setStatus}
        default:
            return state;
    }
};