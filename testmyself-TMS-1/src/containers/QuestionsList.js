import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';
import { setQuiz, setCurrentQuestion, setSelectedOption, setCurrentIndex, toggleSubmit, setOptionStatus } from '../redux/actions/quizActions';
import '../css/QuizCss.css';

const QuestionsList = () => {
    const currentQuestion = useSelector((state) => state.question.currentQuestion);
    const currentIndex = useSelector((state) => state.question.currentIndex);
    const quizData = useSelector((state) => state.quiz.questionsList);
    const questionsLength = useSelector((state) => state.quiz.questionsLength);
    const checkedOptionId = useSelector((state) => state.question.checkedOptionId);
    const submitDisabled = useSelector((state) => state.question.submitDisabled);
    const questionStatus = useSelector((state) => state.question.questionStatus);
    const { quizId } = useParams();
    const dispatch = useDispatch();
    useEffect(() => {
        const { data } = require('../data/quizData.json');
        const quiz = data.filter( quiz => quiz.id == quizId);
        dispatch(setQuiz(quiz[0]));
        dispatch(setCurrentQuestion(quiz[0].questions[0]));
    }, []);
    function nextQuestion() {
        if(currentIndex < (questionsLength-1)){
            dispatch(setCurrentQuestion(quizData.questions[currentIndex+1]));
            dispatch(setCurrentIndex(currentIndex+1));
            dispatch(setSelectedOption(0));
        }
        if(currentIndex < (questionsLength)){
            dispatch(setCurrentIndex(currentIndex+1));
        }
    }
    function checkAnswer(){
        if(checkedOptionId == currentQuestion.correctOptionId){
            dispatch(setOptionStatus({status:'correct',id:checkedOptionId}));
        }
        else{
            dispatch(setOptionStatus({status:'incorrect',id:checkedOptionId}));
        }
        dispatch(toggleSubmit(!submitDisabled));
    }
    function selectedOption(event) {
       dispatch(setSelectedOption(Number(event.target.value)));
       dispatch(toggleSubmit(false));
    }
    let optionsList = currentQuestion.options ? currentQuestion.options.map((optionItem) => {
        return (
            <div key={optionItem.id}>
                <div className='option-title'>
                    <label className='option-label'>
                        <input disabled={optionItem.status != 'unset'} className='option-input' type="radio" name="option_title" value={optionItem.id} checked={checkedOptionId == optionItem.id} onChange={selectedOption}/>{ optionItem.option }
                    </label>
                    {
                        optionItem.status === 'correct' ? <p className='option-result correct-option'>Correct</p> : ``
                    }
                    {
                        optionItem.status === 'incorrect' ? <p className='option-result incorrect-option'>Incorrect</p> : ``
                    }
                </div>
            </div>
        );
    }): ``;
    return (
        <div>
            {
                currentIndex < questionsLength ? 
                <div className='quiz-container'>
                    <div className='question-container'>
                        <p className='question-index'>Question { currentIndex < questionsLength ? (currentIndex+1) : questionsLength }/{ questionsLength }</p>
                        <p className='question-title'>{ currentQuestion.title }</p>
                        <div className='options-container'>{ optionsList }</div>
                    </div>
                    <div className='buttons-container'>
                        {
                            (checkedOptionId != 0 && submitDisabled && questionStatus != 'disable') ?
                                <p>Select another option to try again or move on to next question.</p>
                            : ``
                        }
                        {
                            (checkedOptionId != 0 && submitDisabled) ?
                                <button className='quiz-button' onClick={ nextQuestion }>{currentIndex >= (questionsLength-1) ? 'Done' :'Next Question'}</button>
                            : ``
                        }
                        {
                            !submitDisabled ? 
                            <button className='quiz-button' onClick={ checkAnswer }>Submit</button>
                            : ``
                        }
                        
                    </div>
                </div>
                :
                <div>Completed !!</div>
            }
        </div>
    );
};

export default QuestionsList;