import { combineReducers } from 'redux';
import { quizReducer, questionReducer } from "./quizReducer";

const reducers = combineReducers({
    quiz: quizReducer,
    question: questionReducer,
});

export default reducers;